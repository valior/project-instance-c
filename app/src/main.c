#include <stdlib.h>
#include <stdio.h>

#include "arithmetic.h"

int main()
{
    int a = 0;
    int b = 0;
    int result = 0;

    puts("Hello ISO C arithmetic application");

    int size_of_ptr = sizeof(int*);
    printf("Pointer size is %i \n", size_of_ptr);

    scanf("%i %i", &a, &b);
    result = sum(a, b);
    printf("Sum  is %i \n", result);

    return EXIT_SUCCESS;
}
