#include "arithmetic-tests.h"
#include "arithmetic.h"


START_TEST(Arithmetic_Sum_Success)
{
    int expectedResult = 0;
    int actualResult = 0;

    expectedResult = 4;
    actualResult = sum(2, 2);
    ck_assert_int_eq(expectedResult, actualResult);

    expectedResult = 10;
    actualResult = sum(6, 4);
    ck_assert_int_eq(expectedResult, actualResult);
}
END_TEST

TCase* arithmetic_tests(void)
{
    TCase* tc_arithmetic;

    tc_arithmetic = tcase_create("ArithmeticTests");

    tcase_add_test(tc_arithmetic, Arithmetic_Sum_Success);

    return tc_arithmetic;
}
