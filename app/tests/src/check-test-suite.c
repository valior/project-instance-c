#include "check-test-suite.h"
#include "arithmetic-tests.h"

Suite* create_check_test_suite(void)
{
    Suite* s;

    s = suite_create("CheckTestSuite");

    suite_add_tcase(s, arithmetic_tests());

    return s;
}
