project(tests-${PROJECT_NAME} C)
cmake_minimum_required(VERSION 3.0)

## Tests files
set(tests_headers ${CMAKE_CURRENT_SOURCE_DIR}/include/check-test-suite.h
                  ${CMAKE_CURRENT_SOURCE_DIR}/include/arithmetic-tests.h
)
set(tests_sources ${CMAKE_CURRENT_SOURCE_DIR}/src/check-test-suite.c
                  ${CMAKE_CURRENT_SOURCE_DIR}/src/arithmetic-tests.c
)

## Tests target
add_executable(${PROJECT_NAME} ${project_headers} ${project_sources} ${tests_headers} ${tests_sources} src/main.c)
target_include_directories(${PROJECT_NAME} PRIVATE ../include include)
target_compile_options(${PROJECT_NAME} PRIVATE -Wall -Wextra -pedantic)
target_compile_options(${PROJECT_NAME} PRIVATE $<$<CONFIG:DEBUG>:-DDEBUG -g3 -ggdb3 -O0>)
set_target_properties(${PROJECT_NAME} PROPERTIES C_STANDARD 11)

## Tests dependencies
find_package(PkgConfig REQUIRED)

pkg_check_modules(CHECK check REQUIRED)
target_include_directories(${PROJECT_NAME} PRIVATE ${CHECK_INCLUDE_DIRS})
target_compile_options(${PROJECT_NAME} PRIVATE ${CHECK_CFLAGS})
target_link_libraries(${PROJECT_NAME} ${CHECK_LDFLAGS})

## Run tests command
add_custom_target(
    tests
    COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}
    DEPENDS ${PROJECT_NAME}
)
