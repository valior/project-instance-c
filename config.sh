#!/bin/sh

# Path to project build script (CMakeLists.txt)
scriptdir=$(pwd)/$(dirname $0)


if [ "$#" != "1" ]
then
    echo ""
    echo "Configuration menu. Choose build type by command ./config.sh N"
    echo "\t1. Debug build"
    echo "\t2. Release build"
else
    case "$1" in
    "1")
        cmake $scriptdir -DCMAKE_BUILD_TYPE=Debug;;
    "2")
        cmake $scriptdir -DCMAKE_BUILD_TYPE=Release;;
    esac
fi
